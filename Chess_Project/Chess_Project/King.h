#pragma once
#include <iostream>
#include "Piece.h"

class King : public Piece {
public:	
	const int calcIsMovePossible(const int src, const int dst) override;
	//constructor
	King() {};
	//destructor
	~King() {};

private:
};