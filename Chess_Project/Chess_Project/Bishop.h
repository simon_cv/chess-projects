#pragma once
#include "Piece.h"

class Bishop : virtual public Piece 
{
public:
	virtual const int calcIsMovePossible(const int src, const int dst) override;

	//constructor
	Bishop() {};
	//destructor
	~Bishop() {};
};