#include "Queen.h"

int const Queen::calcIsMovePossible(const int src, const int dst) 
{
    int diagonal = Queen::Bishop::calcIsMovePossible(src, dst);
    int verticalHorizontal = Queen::Rook::calcIsMovePossible(src, dst);

    return diagonal || verticalHorizontal;
}
