#pragma once
#include "Piece.h"
#include "Rook.h"
#include "Bishop.h"

class Queen : public Bishop, public Rook
{
public:
	virtual int const calcIsMovePossible(const int src, const int dst) override;

	//constructor
	Queen() {};
	//destructor
	~Queen() {};
};