#pragma once
#include "Piece.h"
#include <iostream>
#define NUM_OF_PIECES_IN_ROW 8
#define NUM_OF_ROWS 8
#define NUMBER_OF_FRIENDLY_PIECES_MINUS_KING 15

class Board {
private:
	Piece* _board[NUM_OF_ROWS][NUM_OF_PIECES_IN_ROW]; 
	int turn;
	int kingWhite;
	int kingBlack;
	int blackPiecesLocation[NUMBER_OF_FRIENDLY_PIECES_MINUS_KING];
	int whitePiecesLocation[NUMBER_OF_FRIENDLY_PIECES_MINUS_KING];
	
public:
	//field that counts moves 
	static int turnCounter;
	
	//prints Board
	void getBoard();

	//constructor
	Board(std::string& board); //simon
	//destructor
	~Board();
	
	Piece* getPiece(const int position) const;

	/*
	Function: returns the correct response code based on the move the player wants to make
	Input: string - move
	output: int - response code
	*/
	const int returnResponse(const std::string& move);

	/*
	Function: takes a string of a postion on a board and turns it to a postion in a 2-D array
	Input: string - postion as a string
	Output: int - a double digit number first digit is the row second digit is the coloumn in a 2-D array
	*/
	const int convertStringToPosition(const std::string& move) const; //emanual
	
	/*
	Function: checks if the place that the player is trying to move from is his
	Input: int - source position
	Output: bool - answer if the source of the move is valid or not
	*/
	const bool originValid(const int postion) const; //simon

	/*
	Function: calculates the ditance betwwen two blocks on the board and the  diraction from on block to another
	Input: int - source block position, int - destination block position
	Output: int - the diraction to move from the source block to the destination block
	*/
	const int getDistanceAndDiraction(const int start, const int end, int& distance) const; //emanual
	
	/*
	Function: moves the piece in the board
	Input: where to move from and where to move to
	Output: none
	*/
	void movePiece(const int src, const int dst); //simon

	/*
	Function: gets the spot of a king friendly or enemy
	Input: int - is the king to check is the current player's king or the enemy player's king
	Output: bool - the spot of the king to check
	*/
	const int kingToCheckSpot(const int friendlyOrEnemy) const;

	/*
	Function: checks if a king is going to be in check in the current board state
	Input: srcPoint - the position to check for Check on , attackerPoint - the point of the last attacker, attackerDiraction - diraction of the check
	Output: int - is the king in check
	*/
	const int checkForCheck(const int srcPoint, int& attackerPoint, int& attackerDiraction, int color) const; //emanual

	/*
	Function: finds the first obstacle in a spacific diraction from a pawn
	Input: int - source piece's position on the board, int - the steps to move in each iteration, int - max blocks to check in the diraction, 9 the end of the board,attackerPoint - the point of the last attacker, attackerDiraction - diraction of the check
	Output: int - what is the type of obstacle found
	*/
	const int checkDiraction(const int src, const int step, int& attackerPoint,int& attackerDiraction, const int color, int limit = 9) const;

	/*
	Function: a recursive function that checks if any knights can reach a certain point
	Input: int - position to check row, postion to check column, int - stoping number, int color of the piece at the source spot
	Output: bool - is position on the board
	*/
	const int checkForKnights(const int row, const int col, int const count, int const color) const;

	/*
	Function: checks if a given position is in the limits of the board
	Input: int - position to check
	Output: bool - is position on the board
	*/
	const bool postionInBoard(const int postion) const;

	/*
	Function: checks if there is a mate
	Input: king's position
	Output: if there is a check or not
	*/
	const bool checkForMate(const int startPosition);

	/*
	Function: that check if any pieces can block the check
	input: the position of the king to check, the diraction of the check and the color of the king to check 
	output: if the check can be blocked by another piece
	*/
	const bool checkIfCanBlock(const int startPosition,const int attackerPoint,const int attackerDiraction,const int kingToCheckColor);

	/*
	Function: that changes location of piece in arr
	input: arr that the piece is in, the piece to change and its new value
	output: none
	*/
	void changeLocationInArr(int arr[], const int src, const int dst);

	/*
	Function: looks for any pieces in the way to the dstination from the source
	input: int - source position, int destination position, int - int to hold the value of what was found in the way, int - the positon the obstacle was found in, int - the diraction the obstacle was found from
	output: none
	*/
	void checkForObstacles(const int src, const int dst, int& travelState, int& obstaclePoint, int& attackDiraction);

	/*
	Function: updates the array of the pieces's loctions for each side according to the move that was done, and updates the kings's position fields
	input: int - source position, int destination position, Piece pointer - pointer the the piece that was eaten, bool - was en-passment done, int - position of the block near the source position that was eaten in en-passment
	output: none
	*/
	void updateLocationOfPiecesAndKings(const int src, const int dst, Piece* eatenPiece, const bool enPassmentMade, const int pieceToEatNear);
	
	/*
	Function: checks if the piece at the source position can reach the destination position with a legal movement
	input: int - source position, int destination position, bool - was en-passment done, int - can the pawn reach the dst by eating ,int - position of the block near the source position that was eaten in en-passment
	output: none
	*/
	bool checkIfPieceCanReachDest(const int src, const int dst, bool& enpassment, int& canPawnEat, int& pawnOneBlockNear);
};