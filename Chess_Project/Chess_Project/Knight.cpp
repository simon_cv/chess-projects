#include "Knight.h"
#define LEFT_UP_LEFT_OR_RIGHT_DOWN_RIGHT 21
#define UP_LEFT_OR_DOWN_RIGHT 19
#define UP_RIGHT_OR_DOWN_LEFT 12
#define RIGHT_UP_RIGHT_OR_LEFT_DOWN_LEFT 8
 
const int Knight::calcIsMovePossible(const int src, const int dst)
{
    if (abs(src - dst) == LEFT_UP_LEFT_OR_RIGHT_DOWN_RIGHT || abs(src - dst) == UP_LEFT_OR_DOWN_RIGHT || abs(src - dst) == UP_RIGHT_OR_DOWN_LEFT || abs(src - dst) == RIGHT_UP_RIGHT_OR_LEFT_DOWN_LEFT)
        return VALID;
    return INVALID;
}
