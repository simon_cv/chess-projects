#pragma once
#define VALID 1
#define INVALID 0

class Piece{
private:
	int color;

public: 
	/*
	Function: checks if the movment the player wants to commit is valid
	Input: int - source location of the piece on the board, int - destination the player wants to move to
	Output: int - is the movement valid or not
	*/
	virtual const int calcIsMovePossible(const int src, const int dst) = 0;
	
	//constructor
	Piece() {};
	//destructor
	virtual ~Piece() {};

	//return piece color
	int getColor();
	
	/*
	funvtion that sets the color of the piece
	input:new color to set to the piece
	output:none
	*/
	void setColor(const int newColor);
};
