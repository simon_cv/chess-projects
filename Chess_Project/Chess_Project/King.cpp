#include "King.h"

#define RIGHT_DIAGONAL 9
#define LEFT_DIAGONAL 11
#define MOVE_SIDES 1

const int King::calcIsMovePossible(const int src, const int dst) 
{
	int bottomLeft = src + RIGHT_DIAGONAL; //spot to the diagonal left down of the king
	int bottomRight = src + LEFT_DIAGONAL; //spot to the diagonal right down of the king
	int topLeft = src - LEFT_DIAGONAL; //spot to the diagonal left up of the king
	int topRight = src - RIGHT_DIAGONAL; //spot to the diagonal right up of the king
	int left = src + MOVE_SIDES, right = src - MOVE_SIDES; //spots to the left and right of the king

	if ((dst >= bottomLeft && dst <= bottomRight) || (dst >= topLeft && dst <= topRight) || dst == left || dst == right)
		return VALID;
	return INVALID;
}
