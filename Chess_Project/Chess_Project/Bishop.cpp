#include "Bishop.h"
#include "cmath"

#define RIGHT_DIAGONAL 9
#define LEFT_DIAGONAL 11

const int Bishop::calcIsMovePossible(const int src, const int dst) 
{
    int srcRow = src / 10, srcCol = src % 10;
    int dstRow = dst / 10, dstCol = dst % 10;
    int distance = dst - src;

    if (srcRow < dstRow && srcCol < dstCol)
    {
        return distance % LEFT_DIAGONAL == 0;
    }
    else if (srcRow < dstRow && srcCol > dstCol)
    {
        return distance % RIGHT_DIAGONAL == 0;
    }
    else if (srcRow > dstRow && srcCol > dstCol)
    {
        return distance % LEFT_DIAGONAL == 0;
    }
    else if (srcRow > dstRow && srcCol < dstCol)
    {
        return distance % RIGHT_DIAGONAL == 0;
    }
    return INVALID;
}
