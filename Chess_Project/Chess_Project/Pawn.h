#include "Piece.h"
#include "Board.h"
#define EN_PASSMENT_RETURNED 2

class Pawn: public Piece
{
public:
	//constructor
	Pawn();
	//destructor
	~Pawn() {};
	const int calcIsMovePossible(const int src, const int dst) override;
	/*
	function that adds to the move counter 1
	input: pawn to add to 1
	output: none
	*/
	void addMoveCounter();
	/*
	function that checks if a pawn can eat other piece because of his special eating ability
	input:  the src and dst points, the board, and a pointer to var that hold the point of the piece that needs to be eaten in case of en-passment
	output: 1 if the pawn can eat the dst, 0 if it cant and 2 if en-passment was made
	*/
	const int checkIfCanEat(const int src, const int dst, Piece* _board[][NUM_OF_PIECES_IN_ROW], int* pieceToEatOneBlockNear);
	
	/*
	function that sets the moveThatDoubleBlockWasDoneIn field in the pawn if the move is valid and double block movment was done
	input: none
	output: none
	*/
	void setMoveThatDoubleBlockWasDoneIn();

	/*
	function that does the opeartions a Pawn should do in the end of a valid move turn
	input: srcPiece and, pieceToEatOneBlockNear
	output : none
	*/
	void setPawnCounterAndDeleteEnemy(Pawn* srcPiece, Pawn*  pieceToEatOneBlockNear,const int src, const int dst);


private:
	int _moveCounter;
	int _moveThatDoubleBlockWasDoneIn;//needed for en-passment bonus
};

