#include "Board.h"
#include <iostream>
#include <algorithm>
#include <string>
#include "King.h"
#include "Piece.h"
#include "Rook.h"
#include "Queen.h"
#include "Bishop.h"
#include "cmath"
#include "Pawn.h"
#include "Knight.h"

#define MULTIPLIER 10
#define ROW_INDEX 1
#define COLUMN_INDEX 0
#define FIRST_COLUMN 'a'
#define LAST_ROW '8'

#define FRIENDLY_PIECE 3
#define EMPTY 4
#define ENEMY_PIECE 5

#define BOTTOM_LEFT_DIAGONAL 9
#define TOP_RIGHT_DIAGONAL -9
#define BOTTOM_RIGHT_DIAGONAL 11
#define TOP_LEFT_DIAGONAL -11
#define UP -10
#define DOWN 10
#define RIGHT 1
#define LEFT -1

#define WHITE 0
#define BLACK 1

#define ROW_COL_LIMIT_MAX 7
#define ROW_COL_LIMIT_MIN 0

#define CHECK_NOT_POSSIBLE 0
#define CHECK_IS_POSSIBLE 1
#define CHECK_CONFIRMED 2

#define IN_CHECKMATE 1
#define NOT_IN_CHECKMATE 0

#define VALID_MOVE 0
#define VALID_MOVE_CHECK_ENEMY 1
#define INVALID_SOURCE 2
#define INVALID_DESTENATION 3
#define INVALID_MOVE_CHECK_SELF 4
#define INVALID_INDEXES 5
#define INVALID_ILLEGAL_MOVE 6
#define INVALID_UNCHANGING_MOVEMENT 7
#define VALID_ENEMY_CHEKMATE 8
#define VALID_ENPASSMENT_MADE 10

#define CAN_BLOCK true;
#define CANT_BLOCK false;
#define NUMBER_OF_FRIENDLY_PIECES_MINUS_KING 15

#define TWO_BLOCKS_BACKWARDS -20
#define TWO_BLOCKS_FOWARD 20




int Board::turnCounter = 0;

void Board::getBoard()
{
    for (int i = 0; i < NUM_OF_ROWS; i++)
    {
        for (int j = 0; j < NUM_OF_PIECES_IN_ROW; j++)
        {
            if (this->_board[i][j] != nullptr)
            {
                char pieceChar = ' ';

                if (typeid(*(this->_board[i][j])) == typeid(King))//in case of king
                    pieceChar = 'K';

                else if (typeid(*(this->_board[i][j])) == typeid(Rook))//in case of rook
                    pieceChar = 'R';

                else if (typeid(*(this->_board[i][j])) == typeid(Queen))//in case of queen
                    pieceChar = 'Q';

                else if (typeid(*(this->_board[i][j])) == typeid(Bishop))//in case of bishop
                    pieceChar = 'B';

                else if (typeid(*(this->_board[i][j])) == typeid(Knight))//in case of knight
                    pieceChar = 'N';

                else if (typeid(*(this->_board[i][j])) == typeid(Pawn))//in case of pawn
                    pieceChar = 'P';
                
                if (this->_board[i][j]->getColor() == BLACK)
                    pieceChar = tolower(pieceChar);

                std::cout << pieceChar;
            }
            else
            {
                std::cout << "e";
            }
        }
        std::cout << "\n";
    }
}

Board::Board(std::string& board):
    turn(board[size(board) - 1] - 48)
{
    //setting values to -1
    std::fill(std::begin(blackPiecesLocation), std::end(blackPiecesLocation), -1);
    std::fill(std::begin(whitePiecesLocation), std::end(whitePiecesLocation), -1);

    board = board.substr(0, size(board) - 1);
    int counter = 0, blackArrCounter = 0, whiteArrCounter =0;

    for (int i = 0; i < NUM_OF_ROWS; i++)
    {
        for (int j = 0; j < NUM_OF_PIECES_IN_ROW; j++)
        {
            if (isalpha(board[counter]))
            {
                if (board[counter] == 'k')//in case of black king
                {
                    this->_board[i][j] = new King;
                    this->kingBlack = (i * 10 + j);
                    this->_board[i][j]->setColor(BLACK);
                }
                else if (board[counter] == 'K')//in case of white king
                {
                    this->_board[i][j] = new King;
                    this->kingWhite = (i * 10 + j);
                    this->_board[i][j]->setColor(WHITE);
                }

                else if (board[counter] == 'q' || board[counter] == 'Q')//in case of queen
                    this->_board[i][j] = new Queen;

                else if (board[counter] == 'r' || board[counter] == 'R')//in case of rook
                    this->_board[i][j] = new Rook;

                else if (board[counter] == 'b' || board[counter] == 'B')//in case of bishop
                    this->_board[i][j] = new Bishop;

                else if (board[counter] == 'n' || board[counter] == 'N')//in case of pawn
                    this->_board[i][j] = new Knight;

                else if (board[counter] == 'p' || board[counter] == 'P')//in case of pawn
                    this->_board[i][j] = new Pawn;

                if (this->_board[i][j] != nullptr)
                {
                    if(isupper(board[counter]) && board[counter] != 'K')
                    { 
                        this->_board[i][j]->setColor(WHITE);
                        this->whitePiecesLocation[whiteArrCounter] = i * 10 + j;
                        whiteArrCounter++;
                    }
                    else if(board[counter] != 'k' && board[counter] != 'K')
                    {
                        this->_board[i][j]->setColor(BLACK);
                        this->blackPiecesLocation[blackArrCounter] = i * 10 + j;
                        blackArrCounter++;
                    }
                }
            }
            else//empty spot
                this->_board[i][j] = nullptr;
            counter++;
        }
    }
}

Board::~Board()
{
    for (int i = 0; i < NUM_OF_ROWS; i++)
    {
        for (int j = 0; j < NUM_OF_PIECES_IN_ROW; j++)
        {
            delete this->_board[i][j];//freeing memory of allocated pieces
        }
    }

}

Piece* Board::getPiece(const int position) const
{
    return this->_board[position / 10][position % 10];
}

const int Board::returnResponse(const std::string& move)
{
    int src = convertStringToPosition(move.substr(0, 2));
    int dst = convertStringToPosition(move.substr(2, 2));
    int srcRow = src / 10, srcCol = src % 10;
    int dstRow = dst / 10, dstCol = dst % 10;
    int distance = 0, step = 0;
    int travelState = EMPTY;
    int state = 0;
    int kingToCheck = 0;
    bool isMovePossible = false, enPassmentMade = false, isPiecePawn = false, isPieceKnight = false;
    int attackerPoint = 0, attackerDiraction = 0, pieceToEatOneBlockNear = 0, returnValCheckIfCanEat = 0;

    if (!(postionInBoard(src) && postionInBoard(dst)))
        return INVALID_INDEXES;

    if (src == dst)
        return INVALID_UNCHANGING_MOVEMENT;

    if (!originValid(src))
        return INVALID_SOURCE;

    Piece* srcPiece = getPiece(src);
    Piece* dstPiece = getPiece(dst);
    isPiecePawn = typeid(*srcPiece) == typeid(Pawn);

    isMovePossible = checkIfPieceCanReachDest(src, dst, enPassmentMade, returnValCheckIfCanEat, pieceToEatOneBlockNear);

    if (!(isMovePossible))
        return INVALID_ILLEGAL_MOVE;

    checkForObstacles(src, dst, travelState, attackerPoint, attackerDiraction);

    if (travelState != EMPTY)
        return INVALID_DESTENATION;

    Piece* temp = dstPiece;
    movePiece(src, dst);
    kingToCheck = kingToCheckSpot(FRIENDLY_PIECE);

    if (checkForCheck(kingToCheck, attackerPoint, attackerDiraction, getPiece(kingToCheck)->getColor()) != CHECK_NOT_POSSIBLE)
    {
        movePiece(dst, src);
        this->_board[dstRow][dstCol] = temp;
        return INVALID_MOVE_CHECK_SELF;
    }
    turnCounter++;//adding to the turn counter is a move was valid and commited
    updateLocationOfPiecesAndKings(src, dst, temp, enPassmentMade, pieceToEatOneBlockNear);

    if (temp != nullptr)
        temp->~Piece();

    kingToCheck = kingToCheckSpot(ENEMY_PIECE);

    if (typeid(*srcPiece) == typeid(Pawn))//if a move was done by pawn adding it to the pawn move counter
    {
        if (pieceToEatOneBlockNear)
        {
            ((Pawn*)srcPiece)->setPawnCounterAndDeleteEnemy(((Pawn*)srcPiece), ((Pawn*)getPiece(pieceToEatOneBlockNear)), src, dst);
            this->_board[pieceToEatOneBlockNear/ 10][pieceToEatOneBlockNear % 10] = nullptr;
        }
        else
            ((Pawn*)srcPiece)->setPawnCounterAndDeleteEnemy(((Pawn*)srcPiece), nullptr, src, dst);
    }

    this->turn = !this->turn; //change turns

    if (checkForMate(kingToCheck) == IN_CHECKMATE)
    {
        return VALID_ENEMY_CHEKMATE;
    }

    checkForObstacles(dst, kingToCheck, travelState, attackerPoint, attackerDiraction);

    if (travelState == EMPTY && typeid(*this->_board[attackerPoint / 10][attackerPoint % 10]) == typeid(King))
    {
        if(srcPiece->calcIsMovePossible(dst, kingToCheck))
            return VALID_MOVE_CHECK_ENEMY;
        else if(isPiecePawn && ((Pawn*)srcPiece)->checkIfCanEat(dst, kingToCheck, this->_board, &pieceToEatOneBlockNear))
            return VALID_MOVE_CHECK_ENEMY;
    }

    if(returnValCheckIfCanEat != EN_PASSMENT_RETURNED)
        return VALID_MOVE;

    return VALID_ENPASSMENT_MADE;
}

const int Board::convertStringToPosition(const std::string& move) const
{
    int row = LAST_ROW - move[ROW_INDEX];
    row *= MULTIPLIER;
    row += move[COLUMN_INDEX] - FIRST_COLUMN;
    return row;
}

const bool Board::originValid(const int postion) const
{
    int currPlayer = this->turn; //current player color

    if (getPiece(postion) != nullptr && getPiece(postion)->getColor() == currPlayer)//checking if the piece the player is trying to move is his 
        return true;
    return false;

}

const int Board::getDistanceAndDiraction(const int start, const int end, int& distance) const
{
    int srcRow = start / 10, srcCol = start % 10;
    int dstRow = end / 10, dstCol = end % 10;
    distance = end - start;
    int step = 0;

    if (srcRow < dstRow && srcCol < dstCol) // if src is to the top left of dst 
    {
        step = BOTTOM_RIGHT_DIAGONAL;
        distance = abs(distance) / BOTTOM_RIGHT_DIAGONAL;
    }
    else if (srcRow < dstRow && srcCol > dstCol) // if src is to the top right of dst 
    {            
        step = BOTTOM_LEFT_DIAGONAL;
        distance = abs(distance / BOTTOM_LEFT_DIAGONAL);
    }
    else if (srcRow > dstRow && srcCol > dstCol) // if src is to the bottom right of dst 
    {
        step = TOP_LEFT_DIAGONAL;
        distance = abs(distance / TOP_LEFT_DIAGONAL);
    }
    else if (srcRow > dstRow && srcCol < dstCol) // if src is to the bottom left of dst 
    {
        step = TOP_RIGHT_DIAGONAL;
        distance = abs(distance / TOP_RIGHT_DIAGONAL);
    }
    else if(srcRow == dstRow) // if src is on the sane row as dst 
    {
        step = (distance > 0) ? RIGHT : LEFT ;
        distance = abs(distance) / RIGHT;
    }
    else // else the src is on the same column as dst
    {
        distance = abs(distance) / DOWN;
        step = (end - start) / distance;
    }

    return step;
}

void Board::movePiece(const int src, const int dst)
{
    int dstRow = dst / 10, dstCol = dst % 10;
    int srcRow = src / 10, srcCol = src % 10;
    
    this->_board[dstRow][dstCol] = getPiece(src);//making the move
    this->_board[srcRow][srcCol] = nullptr;
    
    if (typeid(*(getPiece(dst))) == typeid(King))
    {
        if (getPiece(dst)->getColor() == WHITE)
            this->kingWhite = dst;
        else
            this->kingBlack = dst;
    }
    
}

const int Board::kingToCheckSpot(const int friendlyOrEnemy) const
{
    int turn = this->turn, sourceSpot = 0;

    if (turn == WHITE && friendlyOrEnemy == FRIENDLY_PIECE)
        sourceSpot = this->kingWhite;
    else if (turn == WHITE && friendlyOrEnemy == ENEMY_PIECE)
        sourceSpot = this->kingBlack;
    else if (turn == BLACK && friendlyOrEnemy == FRIENDLY_PIECE)
        sourceSpot = this->kingBlack;
    else if (turn == BLACK && friendlyOrEnemy == ENEMY_PIECE)
        sourceSpot = this->kingWhite;

    return sourceSpot;
}

const int Board::checkForCheck(const int srcPoint, int& attackerPoint, int& attackerDiraction, int color) const
{
    int diraction = TOP_LEFT_DIAGONAL;
    int src = srcPoint;
    int inCheck = CHECK_NOT_POSSIBLE;
    int checkCounter = 0;

    for (diraction = diraction; checkCounter != CHECK_CONFIRMED && diraction <= BOTTOM_RIGHT_DIAGONAL; diraction++) //check diractions from top left to bottom right
    {
        inCheck = checkDiraction(src, diraction, attackerPoint, attackerDiraction, color);
        if (inCheck == CHECK_IS_POSSIBLE)
        {
            checkCounter += inCheck;
        }

        if (diraction == TOP_RIGHT_DIAGONAL) 
            diraction = LEFT - 1; // diraction will be left in next iteration
        
        else if(diraction == LEFT)
            diraction = RIGHT - 1; // diraction will be right in next iteration
        
        else if(diraction == RIGHT)
            diraction = BOTTOM_LEFT_DIAGONAL - 1; // diraction will be bootom left in next iteration
    }

    inCheck = checkForKnights(src / 10, src % 10, 1, color);
    checkCounter += inCheck;

    return checkCounter;
}

const int Board::checkDiraction(const int src, const int step, int& attackerPoint,int& attackerDiraction, const int color, int limit) const
{
    int dst = src + step;
    int dstRow = dst / 10, dstCol = dst % 10;
    int srcRow = src / 10, srcCol = src % 10;
    int temp;
    attackerPoint = src + step * limit;

    while (postionInBoard(dst) && limit) // while in the board
    {
        if (getPiece(dst) != nullptr) // if there is a piece
        {
            if (getPiece(dst)->getColor() == color) // checks if a friendly piece is blocking the diraction
                return FRIENDLY_PIECE;
            else
            {
                attackerPoint = dst;
                attackerDiraction = step;

                if (getPiece(dst)->calcIsMovePossible(dst, src)) //check if the enemy piece can reach the source piece
                {
                    return CHECK_IS_POSSIBLE;
                }
                else if (typeid(*getPiece(dst)) == typeid(Pawn))
                {
                    Piece* copyBoard[NUM_OF_ROWS][NUM_OF_PIECES_IN_ROW];
                    std::copy(&this->_board[0][0], &this->_board[0][0] + NUM_OF_ROWS * NUM_OF_PIECES_IN_ROW, &copyBoard[0][0]);

                    if(((Pawn*)getPiece(dst))->checkIfCanEat(dst, src, copyBoard, &temp))
                        return CHECK_IS_POSSIBLE;
                }
                else
                    return CHECK_NOT_POSSIBLE;
            }
        }

        dst += step; //change the destination to check
        dstRow = dst / 10, dstCol = dst % 10;
        limit--;
    }

    return EMPTY;;
}

const int Board::checkForKnights(const int row, const int col, int const count, int const color) const
{
    if (count == 0)
    {
        if (postionInBoard(row*10 + col) && this->_board[row][col] != nullptr)
        {
            int otherPieceColor = this->_board[row][col]->getColor();
            if (otherPieceColor != color)
            {
                if (typeid(*(this->_board[row][col])) == typeid(Knight))
                {
                    return CHECK_IS_POSSIBLE;
                }
                else
                {
                    return CHECK_NOT_POSSIBLE;
                }
            }
            else
            {
                return FRIENDLY_PIECE;
            }
        }
        else
        {
            return EMPTY;
        }
    }
    else
    {
        int upRight = checkForKnights(row + 2, col + 1, count - 1, color) == CHECK_IS_POSSIBLE;
        int rightUp = checkForKnights(row + 1, col + 2, count - 1, color) == CHECK_IS_POSSIBLE;
        int upLeft = checkForKnights(row + 2, col - 1, count - 1, color) == CHECK_IS_POSSIBLE;
        int leftUp = checkForKnights(row + 1, col - 2, count - 1, color) == CHECK_IS_POSSIBLE;
        int downRight = checkForKnights(row - 2, col + 1, count - 1, color) == CHECK_IS_POSSIBLE;
        int rightDown = checkForKnights(row - 1, col + 2, count - 1, color) == CHECK_IS_POSSIBLE;
        int downLeft = checkForKnights(row - 2, col - 1, count - 1, color) == CHECK_IS_POSSIBLE;
        int leftDown = checkForKnights(row - 1, col - 2, count - 1, color) == CHECK_IS_POSSIBLE;

        int isThereAknight = upRight || rightUp || upLeft || leftUp || downRight || rightDown || downLeft || leftDown;
        return isThereAknight;
    }
}

const bool Board::postionInBoard(const int postion) const
{
    int row = postion / 10, col = postion % 10;
    bool inRow = row >= ROW_COL_LIMIT_MIN && row <= ROW_COL_LIMIT_MAX;
    bool inCol = col >= ROW_COL_LIMIT_MIN && col <= ROW_COL_LIMIT_MAX;

    return inRow && inCol;
}

const bool Board::checkForMate(const int startPosition)
{
    int attackerDiraction = 0, attackerPoint = 0;
    int srcRow = startPosition / 10, srcCol = startPosition % 10;
    int kingToCheckColor = getPiece(startPosition)->getColor();
    int canEscape = IN_CHECKMATE, dir = TOP_LEFT_DIAGONAL;
    int spotState = CHECK_NOT_POSSIBLE;
    int blockState = CANT_BLOCK;
    int blockNear = 0;

    if (checkForCheck(startPosition, attackerPoint, attackerDiraction, kingToCheckColor))
    {
        for (dir = dir; dir <= BOTTOM_RIGHT_DIAGONAL; dir++) //check diractions from top left to bottom right
        {
            blockNear = startPosition + dir;
            if (postionInBoard(blockNear))
            {
                if ((getPiece(blockNear) != nullptr && getPiece(blockNear)->getColor() != kingToCheckColor) || getPiece(blockNear) == nullptr) //check if the block near is occupied by friendlies
                {
                    spotState = checkForCheck(blockNear, attackerPoint, attackerDiraction, kingToCheckColor);
                    if (spotState == CHECK_IS_POSSIBLE)
                    {
                        blockState = checkIfCanBlock(blockNear, attackerPoint, attackerDiraction, kingToCheckColor);
                        if (blockState)
                        {
                            return NOT_IN_CHECKMATE;
                        }
                    }
                    else if (spotState == CHECK_NOT_POSSIBLE)
                        return NOT_IN_CHECKMATE;
                }
            }

            if (dir == TOP_RIGHT_DIAGONAL)
                dir = LEFT - 1; // diraction will be left in next iteration

            else if (dir == LEFT)
                dir = RIGHT - 1; // diraction will be right in next iteration

            else if (dir == RIGHT)
                dir = BOTTOM_LEFT_DIAGONAL - 1; // diraction will be bootom left in next iteration
        }

        return IN_CHECKMATE;
    }
    else//if we can move the king to cancel the check
        return NOT_IN_CHECKMATE;
}

const bool Board::checkIfCanBlock(const int startPosition,const int attackerPoint,const int attackerDiraction, const int kingToCheckColor)
{
    int numOfBlockToCheck = abs((startPosition - attackerPoint) / attackerDiraction);
    int currBlock = startPosition + attackerDiraction;
    int newAttackerPoint = 0, newAttackerDiraction = 0;
    int travelState = EMPTY, step = 0, distance = 0;
    bool result = CANT_BLOCK;
    int* friendlyPieces = NULL;
    if (kingToCheckColor == WHITE)
        friendlyPieces = this->whitePiecesLocation;

    else
        friendlyPieces = this->blackPiecesLocation;
    
    for (int i = 0; i < numOfBlockToCheck ; i++) //for number of blocks between attacker and king
    {
        currBlock += attackerDiraction;
        if (postionInBoard(currBlock))
        {
            for (int j = 0; j < NUMBER_OF_FRIENDLY_PIECES_MINUS_KING; j++) //for each friendly piece
            {
                if (friendlyPieces[j] != -1)
                {

                    checkForObstacles(friendlyPieces[j], currBlock, travelState, newAttackerPoint, newAttackerDiraction); //check if can reach currBlock

                    if (getPiece(friendlyPieces[j])->calcIsMovePossible(friendlyPieces[j], currBlock) && travelState == EMPTY) //|| (typeid(*this->_board[friendlyPieces[j] / 10][friendlyPieces[j] % 10]) == typeid(Pawn) && ((Pawn*)this->_board[friendlyPieces[j] / 10][friendlyPieces[j] % 10])->checkIfCanEat(friendlyPieces[j], currBlock, this->_board,nullptr)))
                    {
                        Piece* temp = getPiece(currBlock);//moving the piece to check for check
                        this->movePiece(friendlyPieces[j], currBlock);
                        if (checkForCheck(startPosition, newAttackerPoint, newAttackerDiraction, kingToCheckColor) == CHECK_NOT_POSSIBLE)//you cant make a move that will put your king in check
                            result = CAN_BLOCK;

                        this->movePiece(currBlock, friendlyPieces[j]);//moving the piece back
                        if (result)
                            return result;
                    }
                }
            }
        } 
    }
    return result;
}

void Board::changeLocationInArr(int arr[], const int src, const int dst)
{
    for (int i = 0; i < NUMBER_OF_FRIENDLY_PIECES_MINUS_KING; i++)
    {
        if (arr[i] == src)
        {
            arr[i] = dst;
        }
    }
}

void Board::updateLocationOfPiecesAndKings(const int src, const int dst, Piece* eatenPiece, const bool enPassmentMade, const int pieceToEatNear)
{
    Piece* sourcePiece = getPiece(dst);

    if (typeid(*sourcePiece) != typeid(King))//updating location of pieces according to the move if its valid
    {
        if (sourcePiece->getColor() == WHITE)
        {
            changeLocationInArr(this->whitePiecesLocation, src, dst);
            if (eatenPiece != nullptr)
                changeLocationInArr(this->blackPiecesLocation, dst, -1);

            else if (enPassmentMade)
                changeLocationInArr(this->blackPiecesLocation, pieceToEatNear, -1);

        }
        else
        {
            changeLocationInArr(this->blackPiecesLocation, src, dst);
            if (eatenPiece != nullptr)
                changeLocationInArr(this->whitePiecesLocation, dst, -1);

            else if (enPassmentMade)
                changeLocationInArr(this->whitePiecesLocation, pieceToEatNear, -1);
        }
    }
    else if (sourcePiece->getColor() == WHITE)//if the piece is a king updating its place in the field of the board
        this->kingWhite = dst;
    else
        this->kingBlack = dst;

}

bool Board::checkIfPieceCanReachDest(const int src, const int dst, bool& enpassment, int& canPawnEat, int& pawnOneBlockNear)
{

    if (typeid(*getPiece(src)) == typeid(Pawn))
    {
        if (getPiece(src)->calcIsMovePossible(src, dst))
        {
            return true;
        }

        canPawnEat = ((Pawn*)getPiece(src))->checkIfCanEat(src, dst, this->_board, &pawnOneBlockNear);
        if (canPawnEat)//checking if the pawn is trying to eat
        {
            if (canPawnEat == EN_PASSMENT_RETURNED)
                enpassment = true;

            return true;
        }
        else if (getPiece(dst) != nullptr)//if en-passment isnt being done, checking if the pawn is trying to move to a block that isnt null, what means eating the piece in the block in front of him which is impossible
            return false;
    }
    else
    {
        return getPiece(src)->calcIsMovePossible(src, dst);
    }
}

void Board::checkForObstacles(const int src, const int dst, int& travelState, int& obstaclePoint, int& attackDiraction)
{
    int step = 0, distance = 0;
    Piece* sourcePiece = getPiece(src);
    Piece* destPiece = getPiece(dst);

    if (typeid(*sourcePiece) != typeid(Knight))//checking if the piece isnt a knight
    {
        step = getDistanceAndDiraction(src, dst, distance);
        travelState = checkDiraction(src, step, obstaclePoint, attackDiraction, sourcePiece->getColor(), distance);
        if (obstaclePoint != dst)//if the obstacle found is not in the final destination we want to reach
        {
            if (travelState != FRIENDLY_PIECE) //checking enemy pieces or friendly pieces were in the way
                travelState = ENEMY_PIECE;
        }
        else
        {
            travelState = EMPTY; //the way is clear
        }
    }
    else
    {
        obstaclePoint = dst;
        if (destPiece != nullptr)
        {
            if (destPiece->getColor() == sourcePiece->getColor())
                travelState = FRIENDLY_PIECE; //friendly in the destination
            else
                travelState = EMPTY; //the way is clear
        }
        else
            travelState = EMPTY; //the way is clear
    }
}
