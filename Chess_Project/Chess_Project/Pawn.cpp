#include "Pawn.h"
#include "Board.h"
#define TWO_BLOCKS_FOWARD 20
#define CAPTURE_TWO_BLOCK_RIGHT 21 
#define CAPTURE_TWO_BLOCK_LEFT 19
#define ONE_BLOCK_FOWARD 10 
#define TWO_BLOCKS_BACKWARDS -20
#define ONE_BLOCK_BACKWARDS -10 
#define BLACK 1
#define WHITE 0
#define DIAGONAL_LEFT_DOWN -9
#define DIAGONAL_RIGHT_DOWN -11
#define DIAGONAL_RIGHT_UP 9
#define DIAGONAL_LEFT_UP 11
#define ONE_BLOCK_LEFT -1
#define ONE_BLOCK_RIGHT 1
#define ONE_TURN 1

//constructor
Pawn::Pawn()
{
    this->_moveThatDoubleBlockWasDoneIn = 0;
    this->_moveCounter = 0;
}

const int Pawn::calcIsMovePossible(const int src, const int dst)
{
    if (!this->_moveCounter && ((this->getColor() == BLACK && dst - src == TWO_BLOCKS_FOWARD) || this->getColor() == WHITE && dst - src == TWO_BLOCKS_BACKWARDS))//in case that its the pawns first move so he can move 2 blocks foward
        return VALID;
        
    
    else if((this->getColor() == BLACK && dst - src == ONE_BLOCK_FOWARD) || (this->getColor() == WHITE && dst - src == ONE_BLOCK_BACKWARDS))//in case that the first move was already done
        return VALID;
    
    return INVALID;
        
}

void Pawn::addMoveCounter()
{
    this->_moveCounter++;
}

const int Pawn::checkIfCanEat(const int src, const int dst, Piece* _board[][NUM_OF_PIECES_IN_ROW], int* pieceToEatOneBlockNear)
{
    int srcColor = _board[src / 10][src % 10]->getColor();
    if (_board[dst / 10][dst % 10] != nullptr && srcColor != _board[dst / 10][dst % 10]->getColor() || _board[dst / 10][dst % 10] == nullptr)//checking that the pawn isnt trying to go to a block with a friendly piece in it
    {
        if (_board[dst / 10][dst % 10] != nullptr)
            int dstColor = _board[dst / 10][dst % 10]->getColor();
        
        
        if ((srcColor == BLACK && (src - dst == DIAGONAL_LEFT_DOWN || src - dst == DIAGONAL_RIGHT_DOWN)) || (srcColor == WHITE && (src - dst == DIAGONAL_LEFT_UP || src - dst == DIAGONAL_RIGHT_UP)))
        {
            if (_board[src / 10][src % 10 + ONE_BLOCK_LEFT] != nullptr && typeid(*_board[src / 10][src % 10 + ONE_BLOCK_LEFT]) == typeid(Pawn)&&((Pawn*)_board[src / 10][src % 10 + ONE_BLOCK_LEFT])->_moveThatDoubleBlockWasDoneIn == Board::turnCounter && _board[src / 10][src % 10 + ONE_BLOCK_LEFT]->getColor() != srcColor && ((srcColor == BLACK && src - dst == DIAGONAL_LEFT_DOWN) || (srcColor == WHITE && src - dst == DIAGONAL_LEFT_UP)) && typeid(*_board[src / 10][src % 10 + ONE_BLOCK_LEFT]) == typeid(Pawn))//in case of eating the piece one block to the left with en-passment
            {
                if (pieceToEatOneBlockNear != nullptr)
                    *pieceToEatOneBlockNear = src + ONE_BLOCK_LEFT;
                return EN_PASSMENT_RETURNED;
            }
            
            else if (_board[src / 10][src % 10 + ONE_BLOCK_RIGHT] != nullptr && typeid(*_board[src / 10][src % 10 + ONE_BLOCK_RIGHT]) == typeid(Pawn) &&((Pawn*)_board[src / 10][src % 10 + ONE_BLOCK_RIGHT])->_moveThatDoubleBlockWasDoneIn == Board::turnCounter && _board[src / 10][src % 10 + ONE_BLOCK_RIGHT]->getColor() != srcColor && ((srcColor == BLACK && src - dst == DIAGONAL_RIGHT_DOWN) || (srcColor == WHITE && src - dst == DIAGONAL_RIGHT_UP)) && typeid(*_board[src / 10][src % 10 + ONE_BLOCK_RIGHT]) == typeid(Pawn))//in case of eating the piece one block to the right with en-passment
            {
                if(pieceToEatOneBlockNear != nullptr)
                    *pieceToEatOneBlockNear = src + ONE_BLOCK_RIGHT;
                
                return EN_PASSMENT_RETURNED;
            }

            else if(_board[dst /10][dst % 10] != nullptr)
            {
                return VALID;
            }
        }
    }
    return INVALID;
}

void Pawn::setMoveThatDoubleBlockWasDoneIn()
{
    this->_moveThatDoubleBlockWasDoneIn = Board::turnCounter;//this line is right because we only create 1 instance of board so the turnCounter always counts the turns of the current game
}

void Pawn::setPawnCounterAndDeleteEnemy(Pawn* srcPiece, Pawn* pieceToEatOneBlockNear,const int src, const int dst)
{
    if(pieceToEatOneBlockNear != nullptr)
        pieceToEatOneBlockNear->~Pawn();//in case of empasment deleting the piece one block near

    srcPiece->addMoveCounter();//if a move was done adding it to the pawn move counter
    if (src - dst == TWO_BLOCKS_BACKWARDS || src - dst == TWO_BLOCKS_FOWARD)
        srcPiece->setMoveThatDoubleBlockWasDoneIn();
}
