#include "Piece.h"
#include <cmath>

class Knight : public Piece 
{
public:
	const int calcIsMovePossible(const int src, const int dst) override;
	
	//constructor 
	Knight() {};
	//destructor
	~Knight() {};

private:

};
