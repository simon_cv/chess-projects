#include "Rook.h"
#include <iostream>


const int Rook::calcIsMovePossible(const int src, const int dst) 
{
    if ((src % 10 == dst % 10) || (src / 10 == dst / 10))//units of numbers represent their line
        return VALID;
    return INVALID;
}
