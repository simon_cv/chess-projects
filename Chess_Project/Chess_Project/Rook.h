#pragma once
#include <iostream>
#include "Piece.h"


class Rook: virtual public Piece
{
public:
	const int calcIsMovePossible(const int src, const int dst) override;
	//constructor
	Rook() {};
	//destructor
	~Rook() {};

private:
};